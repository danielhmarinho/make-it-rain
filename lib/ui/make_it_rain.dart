import 'package:flutter/material.dart';

class MakeItRain extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MakeItRainState();
  }
    
}

class MakeItRainState extends State<MakeItRain> {
  int _moneyCounter = 0;
  String textColor = 'Colors.greenAccent';
  
  void _rainMoney() {
    setState(() {
      _moneyCounter += 100;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Make it rain!'),
        backgroundColor: Colors.lightGreen,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Center(
              child: Text('Get Rich!',
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.w400,
                  fontSize: 29.9
                ),
              ),
            ),
            Expanded(
              child: Center(
                child: Text('\$$_moneyCounter',
                  style: TextStyle(
                    color: _moneyCounter > 3000 ? Colors.blueAccent : Colors.red,
                    fontSize: 46.9,
                    fontWeight: FontWeight.w800
                  ),
                ),
              ),
            ),
            Expanded(
              child: Center(
                child: FlatButton(
                  color: Colors.lightGreen,
                  textColor: Colors.white70,
                  onPressed: _rainMoney,
                  child: Text('Let it rain!',
                    style: TextStyle(
                      fontSize: 19.9 
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
    
}